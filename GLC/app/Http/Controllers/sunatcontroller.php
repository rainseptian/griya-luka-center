<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\metode;
use App\Models\sunat;
use File;
class sunatcontroller extends Controller
{

    public function _construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $sunat=sunat::all();
        $metode = metode::all();
        $sunat= DB::table('sunat')->get();

        return view('sunat.listsunat',['sunat'=>$sunat,'metode'=>$metode]);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $metodesunat=metode::all();
        return view('sunat.daftar', ['metodesunat'=>$metodesunat]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',
            'usia'=>'required',
            'alamat'=>'required',
            'metodesunat_id'=>'required',
            'image'=>'required|mimes:png,jpg,jpeg|',

        ]);

        $imagename = time().'.'.$request->image->extension();
        $request->image->move(public_path('img'),$imagename);


        DB::table('sunat')->insert([
            'nama'=>$request->input('nama'),
            'usia'=>$request->input('usia'),
            'alamat'=>$request->input('alamat'),
            'metodesunat_id'=>$request->input('metodesunat_id'),
            'image'=>$imagename
        ]);


        return redirect('/sunat');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        // $metode = sunat::with('metode')
        //             ->where('nama', $val)
        //             ->get();
        // return view('sunat.listsunat',['metode'=>$metode]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $metodesunat = metode::all();
        $sunat = DB::table('sunat')->find($id);
        return view('sunat.editsunat',['sunat'=>$sunat, 'metodesunat'=>$metodesunat]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request ,$id)
    {
        $request->validate([
            'nama'=>'required',
            'usia'=>'required',
            'alamat'=>'required',
            'metodesunat_id'=>'required',
            'image'=>'|mimes:png,jpg,jpeg|',
        ]);

       $sunat = sunat::find($id);

       $sunat->nama = $request->nama;
       $sunat->usia = $request->usia;
       $sunat->alamat = $request->alamat;
       $sunat->metodesunat_id = $request->metodesunat_id;

        if ($request->has('image')) {
            $path = 'img/';
            File::delete($path. $sunat->image);

            $newimagename = time().'.'.$request->image->extension();
            $request->image->move(public_path('img'),$newimagename);
            $sunat->image = $newimagename;
        }
        $sunat->save();

        return redirect('/sunat');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        DB::table('sunat')->where('id',$id)->delete();
        return redirect('/sunat');
    }
}
