<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class lukacontroller extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $luka = DB::table('luka')->get();
        return view('luka.luka',['luka'=>$luka]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('luka.tambahluka');

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',

        ]);
        DB::table('luka')->insert([
            'nama'=>$request->input('nama'),

        ]);
        return redirect('/diagnosa');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
