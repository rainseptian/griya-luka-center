<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\diagnosa;
use Illuminate\Support\Facades\DB;

class pasiencontroller extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pasien = DB::table('pasien')->get();
        return view('pages.listpasien',['pasien'=>$pasien]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        return view('pages.tambahpasien');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',
            'usia'=>'required',
            'alamat'=>'required',
            'nik'=>'required',
        ]);
        DB::table('pasien')->insert([
            'nama'=>$request->input('nama'),
            'usia'=>$request->input('usia'),
            'alamat'=>$request->input('alamat'),
            'nik'=>$request->input('nik'),
        ]);
        return redirect('/pasien');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $pasien = DB::table('pasien')->find($id);
        return view('pages.pasienedit',['pasien'=>$pasien]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'nama'=>'required',
            'usia'=>'required',
            'alamat'=>'required',
            'nik'=>'required',
        ]);
        DB::table('pasien')
            ->where('id',$id)
            ->update(
                [
                    'nama'=>$request->input('nama'),
                    'usia'=>$request->input('usia'),
                    'alamat'=>$request->input('alamat'),
                    'nik'=>$request->input('nik'),
                ]
            );
            return redirect('/pasien');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id, Request $request)
    {
        DB::table('pasien')->where('id',$id)->delete();
        return redirect('/pasien');
    }
}
