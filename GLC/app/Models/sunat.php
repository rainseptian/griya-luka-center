<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sunat extends Model
{
    protected $table = 'sunat';
    protected $fillable = ['nama','usia','alamat','metodesunat_id','image'];
    use HasFactory;



}
