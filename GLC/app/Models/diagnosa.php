<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class diagnosa extends Model
{
    protected $table = 'diagnosa';
    protected $fillable = ['nama'];
    use HasFactory;
}
