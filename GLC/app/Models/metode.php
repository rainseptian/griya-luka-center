<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class metode extends Model
{
    protected $table = 'metodesunat';
    protected $fillable = ['nama','harga'];
    use HasFactory;
    public function Sunat()
    {
        return $this->belongsTo(metode::class,'metodesunat_id','id');
    }

}
