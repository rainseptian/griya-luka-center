@extends('pages.master')
@section('title')
    Form Pendaftaran Pasien
@endsection
@section('subtitle')
    Mohon Isi data lengkap
@endsection
@section('content')
<form action="/pasien" method="POST">
    @csrf
    <div class="form-group mx-5">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group mx-5">
        <label>Alamat</label>
        <input type="text" class="form-control" name="alamat">
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group mx-5">
        <label>Usia</label>
        <input type="text" class="form-control" name="usia">
    </div>
    @error('usia')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group mx-5">
        <label>NIK</label>
        <input type="text" class="form-control" name="nik">
    </div>
    @error('nik')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary my-3 ml-5">Daftar</button>
    <a href="{{ URL::previous() }}" class="btn btn-danger">Kembali</a>
    </form>
@endsection
