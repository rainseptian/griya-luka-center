@extends('pages.master')
@section('title')
    Edit Data Pasien
@endsection
@section('subtitle')
    Mohon Isi data lengkap
@endsection
@section('content')
<form action="/pasien/{{$pasien->id}}" method="POST">
    @method('PUT')
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value="{{$pasien->nama}}" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>Alamat</label>
        <input type="text" class="form-control" value="{{$pasien->alamat}}" name="alamat">
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>Usia</label>
        <input type="text" class="form-control" value="{{$pasien->usia}}" name="usia">
    </div>
    @error('usia')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>NIK</label>
        <input type="text" class="form-control" value="{{$pasien->nik}}" name="nik">
    </div>
    @error('nik')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">Kembali</a>
    </form>
@endsection
