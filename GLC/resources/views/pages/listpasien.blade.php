@extends('pages.master');
@section('title')
    List Pasien Rawat Luka
@endsection

@section('content')
<span>
    @auth
        <a href="/pasien/create" class="btn btn-primary my-3">Tambah</a>
    @endauth


</span>
<table class="table table">
    <thead>
      <tr>
        <th scope="col">No.RM</th>
        <th scope="col">Nama</th>
        <th scope="col">Usia</th>
        <th scope="col">Alamat</th>
        <th scope="col">NIK</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($pasien as $key=>$val)
          <tr>
            <td>{{$val->id}}</td>
            <td>{{$val->nama}}</td>
            <td>{{$val->usia}}</td>
            <td>{{$val->alamat}}</td>
            <td>{{$val->nik}}</td>
        @auth
        <td>
            <form action="/pasien/{{$val->id}}" method="POST">
                <a href="/pasien/periksa" class="btn btn-info btn-sm">Periksa</a>
                <a href="/pasien/{{$val->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="delete">
            </form>
        </td>
        @endauth
            </tr>
      @empty
          <tr>
            <td> Tidak ada pasien</td>
          </tr>
      @endforelse
    </tbody>
  </table>
@endsection
