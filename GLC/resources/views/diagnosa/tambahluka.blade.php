@extends('pages.master')
@section('title')
    Tambah diagnosa
@endsection
@section('content')
<form action="/diagnosa" method="POST">
    @csrf
    <div class="form-group mx-5">
        <label class="align-content-center">Nama Diagnosa</label>
        <input type="text" class="form-control" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary btn-sm ml-5 my-3">Tambah</button>
    <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">Kembali</a>
@endsection
