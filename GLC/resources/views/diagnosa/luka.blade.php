@extends('pages.master');
@section('title')
    List Diagnosa
@endsection

@section('content')
<span>
    <a href="/diagnosa/create" class="btn btn-primary my-3 mx-5">Tambah</a>

    </span>
    <table class="table table mx-5">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>

            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($diagnosa as $key=>$val)
              <tr>
                <td>{{$val->id}}</td>
                <td>{{$val->nama}}</td>

            <td>
                <form action="/diagnosa" method="post">
                    <a href="/diagnosa/edit" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
                </tr>
          @empty
              <tr>
                <td> Tidak ada pasien</td>
              </tr>
          @endforelse
        </tbody>
      </table>
@endsection
