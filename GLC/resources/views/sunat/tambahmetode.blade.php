@extends('pages.master')
@section('title')
    tambah metode sunat
@endsection

@section('content')
    <form action="/metodesunat" method="POST">
    @csrf
    <div class="form-group mx-5 ml-5">
        <label class="mr-3">Nama metode</label><br>
        <input type="text" name="nama" class="Form-control @error('title') is-invalid
        @enderror">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group mx-5">
        <label class="mr-5">Harga</label><br>
        <input type="text" name="harga" class="Form-control @error('title') is-invalid
        @enderror">
    </div>
    @error('harga')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary my-4 btn-sm ml-5">Submit</button>
    <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">Kembali</a>
    </form>

@endsection
