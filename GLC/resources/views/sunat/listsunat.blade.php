@extends('pages.master');
@section('title')
    List Pendaftar Sunat
@endsection

@section('content')
<span>
    @auth
        <a href="/sunat/create" class="btn btn-primary my-3">Tambah</a>

    @endauth

</span>
<table class="table table">
    <thead>
      <tr>
        <th scope="col">No.RM</th>
        <th scope="col">Nama</th>
        <th scope="col">Usia</th>
        <th scope="col">Alamat</th>
        <th scope="col">Metode</th>
        <th scope="col">Foto</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($sunat as $key=>$val)
          <tr>
            <td>{{$val->id}}</td>
            <td>{{$val->nama}}</td>
            <td>{{$val->usia}}</td>
            <td>{{$val->alamat}}</td>

            <td>{{$val->metodesunat_id}}</td>


            <td><img src="{{asset('/img/'.$val->image)}}" height="50px" alt=""></td>
            @auth
            <td>
                <form action="/sunat/{{$val->id}}" method="POST">
                    <a href="/sunat/periksa" class="btn btn-info btn-sm">Periksa</a>
                    <a href="/sunat/{{$val->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
            @endauth

            </tr>
      @empty
          <tr>
            <td> Tidak ada data</td>
          </tr>
      @endforelse
    </tbody>
  </table>
@endsection
