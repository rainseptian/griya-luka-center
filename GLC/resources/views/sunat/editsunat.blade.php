@extends('pages.master')
@section('title')
    Edit Data Pasien Sunat
@endsection
@section('subtitle')
    Mohon Isi data lengkap
@endsection
@section('content')
<form action="/sunat/{{$sunat->id}}" method="POST" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value="{{$sunat->nama}}" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>Usia</label>
        <input type="text" class="form-control" value="{{$sunat->usia}}" name="usia">
    </div>
    @error('usia')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>Alamat</label>
        <input type="text" class="form-control" value="{{$sunat->alamat}}" name="alamat">
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>Metode</label>
        <select class="form-control"  name="metodesunat_id" value="{{$sunat->metodesunat_id}}">
        <option value="">----Pilih Metode----</option>
            @forelse ($metodesunat as $item)
                @if ($item->id === $sunat->metodesunat_id)

                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                <option value="{{$item->id}}">{{$item->nama}}</option>

                @endif
            @empty
                <option value="">Tidak ada data</option>
            @endforelse
        </select>
    </div>
    @error('metodesunat_id')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>Foto</label>
        <input type="file" class="form-control" value="{{$sunat->image}}" name="image">
    </div>
    @error('image')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">Kembali</a>
    </form>
@endsection
