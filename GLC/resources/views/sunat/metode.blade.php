@extends('pages.master');
@section('title')
    List Metode Sunat
@endsection

@section('content')
<span>
    <a href="/metodesunat/create" class="btn btn-primary my-3 ml-5 btn-sm">Tambah</a>
    <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">Kembali</a>
    </span>
    <table class="table table mx-5">
        <thead>
          <tr>
            <th scope="col">No.</th>
            <th scope="col">Nama</th>
            <th scope="col">Harga</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($metodesunat as $key=>$val)
              <tr>
                <td>{{$val->id}}</td>
                <td>{{$val->nama}}</td>
                <td>{{$val->harga}}</td>

            <td>
                <form action="/metodesunat" method="POST">
                    <a href="/metodesunat/edit" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
                </tr>
          @empty
              <tr>
                <td> Tidak ada data</td>
              </tr>
          @endforelse

        </tbody>

      </table>

@endsection
