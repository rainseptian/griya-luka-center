@extends('pages.master')
@section('title')
    Form Pendaftaran Sunat
@endsection
@section('subtitle')
    Mohon Isi data lengkap
@endsection
@section('content')

<form action="/sunat" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group mx-5">
        <label>Nama Lengkap</label>
        <input type="text" class="form-control" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group mx-5">
        <label>Alamat</label>
        <input type="text" class="form-control" name="alamat">
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group mx-5">
        <label>Usia</label>
        <input type="text" class="form-control" name="usia">
    </div>
    @error('usia')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group mx-5">
        <label>Metode Sunat</label>
        <select name="metodesunat_id" id="" class="form-control">
            <option value="">----Pilih Metode----</option>
            @forelse ($metodesunat as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @empty
                <option value="">Tidak ada data</option>
            @endforelse
        </select>
    </div>
    @error('metodesunat_id')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group mx-5">
        <label>Foto</label>
        <input type="file" class="form-control -mr-3" name="image">
    </div>
    @error('image')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary my-3 ml-5">Daftar</button>
    <a href="{{ URL::previous() }}" class="btn btn-danger">Kembali</a>
</form>
@endsection
