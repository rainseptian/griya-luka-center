<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\pasiencontroller;
use App\Http\Controllers\metodecontroller;
use App\Http\Controllers\rawatcontroller;
use App\Http\Controllers\lukacontroller;
use App\Http\Controllers\sunatcontroller;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
});


Auth::routes();
Route::middleware('auth')->group(function(){
    Route::resource('metodesunat', metodecontroller::class);
    Route::resource('diagnosa', rawatcontroller::class);
    //Route::resource('profile', profile::class);
    Route::resource('pasien', pasiencontroller::class);
    Route::resource('sunat',sunatcontroller::class);
});
